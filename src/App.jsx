import React, { useEffect, useState } from "react";
import TodoForm from "./components/TodoForm";
import TodoList from "./components/TodoList";
import { useMouseTracker } from "./hooks/useMouseTracker";

function App() {
  const { x, y } = useMouseTracker();
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    document.title = `Todos ${todos.length}`;
  }, [todos]);

  function handleFormSubmit(value) {
    setTodos((prevTodos) => {
      return [...prevTodos, value];
    });
  }

  function handleDelete(index) {
    const oldTodos = todos.slice();
    oldTodos.splice(index, 1);
    setTodos(oldTodos);
  }

  return (
    <div className="w-3/5 mx-auto mt-5">
      <h1 className="text-lg font-bold text-center my-2">Todo App</h1>
      <TodoForm onSubmit={handleFormSubmit} />
      <TodoList todos={todos} onDelete={handleDelete} />
      <h1 className="tracking-wide text-xl font-bold text-center">
        X is {x} and Y is {y}
      </h1>
    </div>
  );
}

// class App extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       todos: [],
//     };

//     this.handleFormSubmit = this.handleFormSubmit.bind(this);
//     this.handleDelete = this.handleDelete.bind(this);
//   }

//   handleFormSubmit(todoInput) {
//     this.setState(
//       (prevState) => {
//         return {
//           todos: [...prevState.todos, todoInput.value],
//         };
//       },
//       () => {
//         todoInput.value = "";
//       }
//     );
//   }

//   handleDelete(index) {
//     this.setState((prevState) => {
//       const oldTodos = prevState.todos.slice();
//       oldTodos.splice(index, 1);
//       return {
//         todos: oldTodos,
//       };
//     });
//   }

//   render() {
//     const { todos } = this.state;

//     return (
//       <div className="w-3/5 mx-auto mt-5">
//         <h1 className="text-lg font-bold text-center my-2">Todo App</h1>
//         <TodoForm onSubmit={this.handleFormSubmit} />
//         <TodoList todos={todos} onDelete={this.handleDelete} />

//         <MouseTracker>
//           {({ x, y }) => (
//             <h1 className="tracking-wide text-xl font-bold text-center">
//               X is {x} and Y is {y}
//             </h1>
//           )}
//         </MouseTracker>
//       </div>
//     );
//   }
// }

export default App;
