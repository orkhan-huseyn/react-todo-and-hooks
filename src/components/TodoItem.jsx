import React from "react";

function TodoItem({ text, index, onDelete }) {
  return (
    <li className="py-3 flex justify-between px-4">
      <span>{text}</span>
      <button onClick={() => onDelete(index)} className="bg-red">
        X
      </button>
    </li>
  );
}

export default TodoItem;
