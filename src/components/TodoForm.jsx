import React from "react";

function TodoForm({ onSubmit }) {
  function handleSubmit(event) {
    event.preventDefault();
    const { todoInput } = event.target.elements;
    if (!todoInput.value) return;
    onSubmit(todoInput.value);
    todoInput.value = "";
  }

  return (
    <form onSubmit={handleSubmit} autoComplete="off" className="flex mb-5">
      <input
        className="rounded-lg basis-4/5 font-semibold border border-slate-300 mr-2 p-3"
        placeholder="Add todo..."
        aria-label="Add todo input"
        name="todoInput"
      />
      <button className="button">Add</button>
    </form>
  );
}

export default TodoForm;
