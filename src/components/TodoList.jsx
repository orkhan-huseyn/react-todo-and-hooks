import React from "react";
import TodoItem from "./TodoItem";

function TodoList({ todos, onDelete }) {
  return (
    <ul className="w-full divide-y divide-slate-200 text-sm font-medium text-gray-900 bg-white rounded-lg border border-gray-200 ">
      {todos.map((todoText, index) => (
        <TodoItem
          key={index}
          index={index}
          text={todoText}
          onDelete={onDelete}
        />
      ))}
    </ul>
  );
}

export default TodoList;
